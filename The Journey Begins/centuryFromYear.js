function centuryFromYear(year) {
   var x;
   if (year % 100 == 0) {
      x = year / 100;
   } else {
      x = parseInt(year / 100) + 1;
   }
   return x;
}
