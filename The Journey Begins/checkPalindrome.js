function checkPalindrome(inputString) {
    for (var i = 1; i <= inputString.length/2; i++) {
        if (inputString.charAt(i-1) != inputString.charAt(inputString.length-i)) {
           return false;
       }
   }
   return true;
}

