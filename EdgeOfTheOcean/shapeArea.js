function shapeArea(n) {
    let s = 1; //
    for (i = 1; i < n ; i++){
       s += i * 4;
    }
    return s;
}
// 2 => 1*4+1
// 3 => 2*4+1*4 + 1
// 4 => 3*4+2*4 +1 *4 +1 = 12+8+4+1 = 25
//  